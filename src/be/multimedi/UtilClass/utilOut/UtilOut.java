package be.multimedi.UtilClass.utilOut;

public class UtilOut {

    public static void mainTitle(String title) {
        System.out.println(title);
        for (int i = 0; i < title.length();i++) {
            System.out.println("=");
        }
        System.out.println();
    }

    public static void Title(String title) {
        System.out.println(title);
        for (int i = 0; i < title.length();i++) {
            System.out.println("-");
        }
        System.out.println();
    }

    public static void printPhrase(String phrase) {
        System.out.println(phrase+"\n");
    }

    public static void printInt(int numbers) {
        System.out.println(numbers+"\n");
    }

    //TODO generic printer
}
