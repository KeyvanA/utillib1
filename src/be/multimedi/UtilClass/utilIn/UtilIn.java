package be.multimedi.UtilClass.utilIn;

import java.util.Scanner;

public class UtilIn {

    public static int askForInt(String question) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(question);
        return keyboard.nextInt();
    }

    public static String askForString(String question) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(question);
        return keyboard.nextLine();
    }

    public static double askForDouble(String question) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(question);
        return keyboard.nextDouble();
    }

    public static float askForFloat(String question) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(question);
        return keyboard.nextFloat();
    }

    public static Long askForLong(String question) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println(question);
        return keyboard.nextLong();
    }
}
